# README #

This README would normally document whatever steps are necessary to get your application up and running.

#### Prerequisites
1. Linux system (eg. Ubuntu 16.04 LTA or Latest)
2. Latest g++ compiler 

### What is this repository for? ###
This application is to list all the libraries file in a directory and their compatible machine information

### How do I get set up? ###

##To compile execute the following command:

cd /path/to/listlibinfo

make 

To compile in debug mode 

make debug=1

It will generate a exe file listlibinfo in listlibinfo/builds/bin folder

## To run this execute the following command 

./build/bin/listlibinfo <path/to/libDir>

