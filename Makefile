EXE_DIR= ./build/bin

CC= /usr/bin/g++
DEBUG=$(debug)
CFLAGS= -I ./inc 

ifeq ($(DEBUG),1)
	CFLAGS += -g -Wall -DDEBUG
endif


SRC=$(wildcard ./src/*.cpp)
TARGET= $(EXE_DIR)/listLibraryInfo

.PHONY : all clean

all:TARGET

TARGET:
	${CC} -o $(TARGET) $(SRC) $(CFLAGS)
clean :
	rm -rf ${TARGET}
