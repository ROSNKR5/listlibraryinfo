#include "listLibraryInfo.h"

inline bool isDirectoryExists(const std::string& directory_path)
{
	struct stat stat_buff;
	bool retval = 0;
	if (stat(directory_path.c_str(),  &stat_buff))
		retval = 0;
	else if(stat_buff.st_mode & S_IFDIR){
		retval = true;
	}
	return retval;
}
inline bool isLibraryFile (const std::string& filename) {
	struct stat buffer; 
	bool retval = 0;  
	if (stat (filename.c_str(), &buffer) == 0) {
		switch (buffer.st_mode & S_IFMT) {
			case S_IFBLK:   retval = 0;       break;
			case S_IFCHR:   retval = 0;       break;
			case S_IFDIR:   retval = 0;       break;
			case S_IFIFO:   retval = 0;       break;
			case S_IFSOCK:  retval = 0;       break;
			default:        retval = true;    break;
		}
	}

	return retval;
}
char * read_elf_header(const char* elfFile ) {
	Elf64_Ehdr header;
	char *result = NULL;
	FILE* file = fopen(elfFile, "rb");
	if(file) {
		if( fread(&header, 1, sizeof(header), file) < sizeof(header) ) {
#ifdef DEBUG
			cout << "Error: Not a Vailid library file:" << elfFile << "\n\n";
#endif
			return result;
		}
			

		if (memcmp(header.e_ident, ELFMAG, SELFMAG) == 0) {
#ifdef DEBUG
			cout << "\nThis is a valid elf file :" << elfFile <<endl;
			cout <<  "\nClass  :                    " << elf_ident_info[EI_CLASS][(int)(header.e_ident[EI_CLASS])];
			cout <<  "\nData   :                    " << elf_ident_info[EI_DATA][(int)(header.e_ident[EI_DATA])]; 
			cout <<  "\nOS/ABI :                    " << elf_ident_info[EI_OSABI][(int)(header.e_ident[EI_OSABI])];
			cout <<  "\nType   :                    " << elf_type_info[(int)(header.e_type)];
			cout <<  "\nMachine:                    " << elf_machine_info[(int)(header.e_machine)] << "\n";
#endif
			result = elf_machine_info[(int)(header.e_machine)]; 
		}
		else 
#ifdef DEBUG
			cout << "\nThis is not a valid elf file:" << elfFile << "\n\n";
#endif
		fclose(file);
	}
	return result;
}

int listDirectory (string dir, vector<string> &files)
{
    DIR *dirHandler;
    struct dirent *st_dirp;
    if((dirHandler  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((st_dirp = readdir(dirHandler)) != NULL) {
		//cout <<"file name is: " << dir+st_dirp->d_name <<"\n";		
		if(isLibraryFile(dir + st_dirp->d_name))
			files.push_back(string(st_dirp->d_name));
    }
    closedir(dirHandler);
    return 0;
}
void usage(void)
{
	cout << "\n\n Please Provide a Vaild Directory Path\n\n";
}
void printResult(vector<string> &result, int count)
{
	if(count) {
		cout << "\nTotal number of Libraris:    " << count;
		cout << "\n\nFile:                      Architecture: \n";
		cout << "==========                ================\n\n";
		for (unsigned int i = 0;i < result.size();i++) {
			cout << result[i] << endl;
		}
	}
	else
		cout << "\nNo any library found in this folder\n\n";
}

int main(int argc, char *argv[])
{
	char* libInfo;
	int count = 0;
	string directory = "";
    vector<string> files = vector<string>();
    vector<string> result = vector<string>();

	if ((argc > 1 )&& (isDirectoryExists(argv[1]))) {
		directory = argv[1];
	}
	else{
		usage();
		return 0;
	}
	if( listDirectory(directory,files) == 0) {

		for (unsigned int i = 0;i < files.size();i++) {
			//cout << files[i] << endl;
			if (libInfo = read_elf_header((directory + files[i]).c_str())) {
				result.push_back((files[i]+"                     "+libInfo).c_str());
				count++;
			}
		}
		printResult(result, count);
	}
    return 0;
}
